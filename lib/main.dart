import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  home: Home(),
));



class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cure sua Cegueira!'),
        centerTitle: true,
        backgroundColor: Colors.indigo[700],
      ), // (widgets usam PascalCase)
      body: Container(
        padding: EdgeInsets.fromLTRB(70.0, 356.0, 70.0, 356.0),
        color: Colors.grey[700],
        child: ElevatedButton.icon(
          onPressed: () {},
          icon: Icon(Icons.adb_outlined,
          size: 75.0,
          color: Colors.amber[700],
          ),
          label: Text('Mim aperta aqui pu ce ve...',
            style: TextStyle(
              fontFamily: 'Pangolin',
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 1.5,
            )
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Colors.red[600],
        child: Text('Mim aperte',
          style: TextStyle(
            fontFamily: 'Pangolin',
            fontWeight: FontWeight.bold,
          ),
        textAlign: TextAlign.center),
      ),
    );
    //virgula após valor da propriedade;
  }
}